import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeAudio } from '@ionic-native/native-audio';
import { Network } from '@ionic-native/network';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { JuegoPage } from '../pages/juego-page/juego-page';
import { LoginPage } from '../pages/login-page/login-page';
import { CursosPage } from '../pages/cursos-page/cursos-page';
import { ModulosPage } from '../pages/modulos-page/modulos-page';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { RankingPage } from '../pages/ranking-page/ranking-page';
import { PerfilPage } from '../pages/perfil-page/perfil-page';
import { PartidaPage } from '../pages/partida-page/partida-page';
import { RankingListadoPage } from '../pages/rankingListado-page/rankingListado-page';
import { AsignacionCursosPage } from '../pages/asignacion-cursos/asignacion-cursos';
import { FiltroCargosPage } from '../pages/filtro-cargos/filtro-cargos';
import { MisCursos } from '../pages/mis-cursos/mis-cursos';

import { FlashCard } from '../components/flash-card/flash-card';
import { ProgressBar } from '../components/progress-bar/progress-bar';

import { Data } from '../providers/data';
import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';

import { CloudSettings, CloudModule } from '@ionic/cloud-angular';


const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '898e377f',
  },
  'push': {
    'sender_id': '231347820381',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    JuegoPage,
    LoginPage,
    CursosPage,
    ModulosPage,
    TabsPage,
    RankingPage,
    PerfilPage,
    PartidaPage,
    RankingListadoPage,
    AsignacionCursosPage,
    FiltroCargosPage,
    MisCursos,
    FlashCard,
    ProgressBar
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Volver',
    }),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    JuegoPage,
    LoginPage,
    CursosPage,
    ModulosPage,
    TabsPage,
    RankingPage,
    PerfilPage,
    PartidaPage,
    RankingListadoPage,
    AsignacionCursosPage,
    FiltroCargosPage,
    MisCursos
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Data,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConnectivityServiceProvider,
    Network,
    NativeAudio
  ]
})
export class AppModule {}
