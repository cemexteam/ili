import { Component, Input } from '@angular/core';

/**
 * Generated class for the ProgressBar component.
 */

@Component({
  selector: 'progress-bar',
  templateUrl: 'progress-bar.html'
})
export class ProgressBar {

  @Input('progress') progress;

  constructor() {

  }

}
