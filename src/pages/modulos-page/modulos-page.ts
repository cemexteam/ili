import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, App, LoadingController, Platform, Slides, Content, Events } from 'ionic-angular';
import { PartidaPage } from '../partida-page/partida-page';
import { Data } from '../../providers/data'

import { NativeAudio } from '@ionic-native/native-audio';
/**
 * Generated class for the SeguridadModulosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 
@Component({
  selector: 'page-modulos-page',
  templateUrl: 'modulos-page.html',
})
export class ModulosPage {

	@ViewChild(Content) content: Content;
	@ViewChild(Slides) slides: Slides;

	url : string;
	idCurso : string;
	listaModulos : any;
	idUsuario : any;
	listaModulosUsuario : any;

	loading = this.loadingCtrl.create({
		content: 'Cargando módulos...'
	});

  constructor(platform: Platform, public navCtrl: NavController, public navParams: NavParams, private servicio: Data, private app: App, public loadingCtrl: LoadingController, private nativeAudio: NativeAudio , public events: Events) {

  	this.idCurso =  this.navParams.get('idCurso');
  	this.idUsuario =  JSON.parse(localStorage.getItem('usuarioDocumento'));

	events.subscribe('reloadModules', (idCur) => {

	this.loading = this.loadingCtrl.create({
			content: 'Cargando...'
		});		

    this.verListaModulos();
  });


  }

  	ionViewDidEnter(){

		this.loading = this.loadingCtrl.create({
			content: 'Cargando...'
		});		



	  	this.verListaModulos();
	  	this.nativeAudio.preloadSimple('clickModulo', 'assets/sounds/clickModulo.mp3');

	}

	verListaModulos(){
		//console.log(this.idCurso);
		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/modulos/" + this.idCurso;

		this.servicio.obtenerModulos(this.url)
			.subscribe(
				(data) => {
					this.listaModulos = data.Modulos;
				},
				er => console.log(er),
				() => {
					// console.log(this.listaModulos);
					this.verificacionModulos();
				}
			)	
	}

	verificacionModulos() {
		this.url = "https://cemexapp-168019.appspot.com/api/ili/modulosCursoConPuntaje/" + this.idUsuario + "/" + this.idCurso;

		this.servicio.obtenerModulos(this.url)
			.subscribe(
				(data) => {
					this.listaModulosUsuario = data.Modulos;
				},
				er => console.log(er),
				() => {
					this.habilitarModulos();
					this.loading.dismiss();
				}
			)
	}

	habilitarModulos() {

		let index = 0; 

		this.listaModulos[0].habilitado = true;
		for (var i = 0; i < this.listaModulos.length-1; i++) {
			let modulo = this.listaModulos[i].id;
			for (var j = 0; j < this.listaModulosUsuario.length; j++) {
				if (modulo == this.listaModulosUsuario[j].MODULO_id && this.listaModulosUsuario[j].valor >= 80) {
					this.listaModulos[i+1].habilitado = true;
					this.listaModulosUsuario.splice(j, 1);
					index++;
					break;
				}
			}
		}

		this.slides.slideTo(index);

	}

	verPreguntas(modulo, nombre) {
		this.nativeAudio.play('clickModulo');
		
		let data = {
			idModulo : modulo,
			nombreModulo : nombre,
			idCurso : this.idCurso,
			parentPage : ModulosPage
		};
		this.app.getRootNav().push(PartidaPage, data);
	}	
}
