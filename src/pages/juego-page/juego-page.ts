import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { CursosPage } from '../cursos-page/cursos-page';

import { NativeAudio } from '@ionic-native/native-audio';

@Component({
  selector: 'page-juego-page',
  templateUrl: 'juego-page.html'
})
export class JuegoPage {

  constructor(platform: Platform, public navCtrl: NavController, private nativeAudio: NativeAudio) {

    this.nativeAudio.preloadSimple('clickDesafio', 'assets/sounds/clickDesafio.mp3');

  }

  verCursosPage(categoria): void {

    this.nativeAudio.play('clickDesafio');

  	let data = {
  		idCategoria : categoria
  	};

  	this.navCtrl.push(CursosPage, data);
  }

}
