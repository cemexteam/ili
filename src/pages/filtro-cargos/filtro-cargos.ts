import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the FiltroCargosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-filtro-cargos',
  templateUrl: 'filtro-cargos.html',
})
export class FiltroCargosPage {

	cargos: any; //Lista del nombre de los cargos obtenidos de la vista preliminar de los usuarios
	cargosLista: any = []; //Arreglo de objetos de los cargos con atributos nombre y checked
	cargosFiltro: any = []; //Arreglo de los cargos que se devuelven para el filtro en la vista preliminar (colaboradores)
	cargosSearch: any; //Lista utilizada para presentar los cargos filtrados

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
		this.cargos = navParams.get('cargos');
		for (var i = 0; i < this.cargos.length; ++i) {
			this.cargosLista.push({
				nombre : this.cargos[i],
				checked : false
			});
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad FiltroCargosPage');
	}

	dismiss() {
		let data = { 'filtroCursos': this.cargosFiltro };
		this.viewCtrl.dismiss(data);
	}

	initializeItems() {
		this.cargosSearch = this.cargosLista;
	}

	getItems(ev) {
		this.initializeItems();

		let val = ev.target.value;

		if (val) { 
			if (val && val.trim() != '') {
				this.cargosSearch = this.cargosSearch.filter((item) => {
					return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
				})
			}
		}
		else {
			this.cargosSearch = [];
		}
	}

	seleccionarCargo(e:any, cargo) {
		let index: number = -1;

		if (e.checked) {
			cargo.checked = true;
			this.cargosFiltro.push(cargo);
		} else {
			cargo.checked = false;
			index = this.cargosFiltro.findIndex(item => item.nombre === cargo.nombre);
			if (index > -1) {
			    this.cargosFiltro.splice(index, 1);
			}
		}
	}

	cancelarBusqueda(){

		let vacio: any = [];
		let data = { 'filtroCursos': vacio };
		this.viewCtrl.dismiss(data);
	}
}
