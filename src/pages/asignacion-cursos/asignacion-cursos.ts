import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';

import { FiltroCargosPage } from '../filtro-cargos/filtro-cargos';

import { Data } from '../../providers/data'

/**
 * Generated class for the AsignacionCursosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-asignacion-cursos',
  templateUrl: 'asignacion-cursos.html',
})
export class AsignacionCursosPage {

	url: string;
	asignacion: string = "cursos";
	cursos: any;
	cursosSearch: any = [];
	usuarios: any;
	usuariosSearch : any = [];
	cursosAsignados: any = [];
	todosUsuariosSeleccionados: boolean = false;
	todosCursosSeleccionados: boolean = false;

	cargos: any = [];

	loading = this.loadingCtrl.create({
		content: 'Cargando...'
	});

	constructor(public navCtrl: NavController, public navParams: NavParams, private servicio: Data, private alertCtrl: AlertController, public loadingCtrl: LoadingController, public modalCtrl: ModalController) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AsignacionCursosPage');
	}

	ionViewDidEnter() {
		this.verListaCursos();
	}

	verListaCursos(){
		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/cursos";
		this.servicio.obtenerCursos(this.url)
			.subscribe(
				(data) => {
					this.cursos = data.Cursos;
				},
				er => console.log(er),
				() => {
					for (var i = 0; i < this.cursos.length; ++i) {
						this.cursos[i].checked = false;
					}
					this.verUsuarios();
				}
			)	
	}

	verUsuarios(){
		this.url = "https://cemexapp-168019.appspot.com/api/ili/usuarios";
		this.servicio.obtenerUsuarios(this.url)
			.subscribe(
				(data) => {
					this.usuarios = data.Usuarios;
				},
				er => console.log(er),
				() => {
					for (var i = 0; i < this.usuarios.length; ++i) {
						this.usuarios[i].checked = false;
					}
					this.cursosSearch = this.cursos;
					this.filtrarCargos();
					this.loading.dismiss();
				}
			)	
	}

	filtrarCargos() {
		this.cargos = this.usuarios.map(item => item.cargo)
  			.filter((value, index, self) => self.indexOf(value) === index);
	}

	initializeItems() {
		this.cursosSearch = this.cursos;
		this.usuariosSearch = this.usuarios;
	}

	getItems(ev, asignacion) {
		this.initializeItems();

		let val = ev.target.value;

		if (val) { 
			if (asignacion == "cursos") {
				if (val && val.trim() != '') {
					this.cursosSearch = this.cursosSearch.filter((item) => {
						return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
					})
				}
			}
			if (asignacion == "colaboradores") {
				if (val && val.trim() != '') {
					this.usuariosSearch = this.usuariosSearch.filter((item) => {
						return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.apellido.toLowerCase().indexOf(val.toLowerCase()) > -1);
					})
				}
			}
		} else {
			this.usuariosSearch = [];
		}
	}

	seleccionarCurso(e:any, curso) {

		if (e.checked) {
			curso.checked = true;
			//console.log(curso);
		} else {
			curso.checked = false;
			//console.log(curso);
		}
	}

	seleccionarUsuario(e:any, usuario) {

		if (e.checked) {
			usuario.checked = true;
			//console.log(usuario);
		} else {
			usuario.checked = false;
			//console.log(usuario);
		}
	}

	cancelarAsignacion() {
		this.navCtrl.pop();
	}

	confirmarAsignacion() {

		let seleccionUsuarios: any = [];
		let resultado: any = [];
		
		this.cursosAsignados  = this.cursosSearch.filter((item) => {
						return (item.checked == true);
					});

		seleccionUsuarios  = this.usuarios.filter((item) => {
						return (item.checked == true);
					});

		resultado = this.generarAsignacion(this.cursosAsignados, seleccionUsuarios)

		if (resultado.length > 0 ){
			this.asignarCursos(resultado);
		} else {
			this.alertErrorAsignacion();
		}
	}

	generarAsignacion(cursos, usuarios){

		var resultadoAsignaciones: any = [];
		var asignacion: any = [];


		for (var i = 0; i < cursos.length; ++i) {
			for (var j = 0; j < usuarios.length; ++j) {
				asignacion.push( usuarios[j].n_documento )
				asignacion.push( cursos[i].id )
				//asignar habilitado
				resultadoAsignaciones.push( asignacion );
				asignacion = [];				
			}
		}

		return resultadoAsignaciones;
	}

	presentarFiltroPosicionModal() {
		let filtroPosicionModal = this.modalCtrl.create(FiltroCargosPage, { cargos: this.cargos });
		filtroPosicionModal.onDidDismiss(data => {
			this.filtrarUsuariosCargo(data);
		});
		filtroPosicionModal.present();
	}

	filtrarUsuariosCargo(cargos) {
		// this.initializeItems();
		this.usuariosSearch = [];
		let usuariosAux: any = [];

		for (var i = 0; i < cargos.filtroCursos.length; ++i) {
			usuariosAux = this.usuarios.filter((item) => {
						return (item.cargo.toLowerCase().indexOf(cargos.filtroCursos[i].nombre.toLowerCase()) > -1);
					});
			//console.log(usuariosAux);
			this.usuariosSearch = this.usuariosSearch.concat(usuariosAux);
			//console.log(this.usuariosSearch);
		}
	}

	seleccionarTodoOQuitarseleccion( asignacion ){
 
		let auxSeleccion : any;

		if(asignacion == "colaboradores"){

			auxSeleccion = !this.todosUsuariosSeleccionados;
			this.todosUsuariosSeleccionados = auxSeleccion;

			for (var i = 0; i < this.usuariosSearch.length; ++i) {

				this.usuariosSearch[i].checked = this.todosUsuariosSeleccionados;

			}

		} else if (asignacion == "cursos"){

			auxSeleccion = !this.todosCursosSeleccionados;
			this.todosCursosSeleccionados = auxSeleccion;

			for (var i = 0; i < this.cursosSearch.length; ++i) {
				
				this.cursosSearch[i].checked = this.todosCursosSeleccionados;
			}
		}
	}

	alertErrorAsignacion() {

		let alert = this.alertCtrl.create({
			title:"Error de asignación",
			message:"Seleccione al menos un curso y un colaborador",
			buttons: ['Ok']

		});

		alert.present();
	}	

	asignarCursos(resultado) {
		let error: Boolean = false;
		let mensaje: string = "";
		this.url = "https://cemexapp-168019.appspot.com/api/ili/asignacionCursos";
		this.servicio.asignarCursos(this.url, resultado)
			.subscribe(
				(data) => {
					error = data.Error,
					mensaje = data.Message
				},
				er => console.log(er),
				() => {
					if (error) { 
						console.log(mensaje);
					} else {
						//console.log(mensaje);
						this.navCtrl.pop();
					}
				}
			)	
	}
}
