import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, AlertController, Platform, LoadingController, Content, Events  } from 'ionic-angular';
import { Data } from '../../providers/data';
import { TabsPage } from '../tabs-page/tabs-page';
import { ModulosPage } from '../modulos-page/modulos-page';
import { NativeAudio } from '@ionic-native/native-audio';

/**
 * Generated class for the PartidaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-partida-page',
  templateUrl: 'partida-page.html',
})
export class PartidaPage {

  @ViewChild(Content) content: Content;
	@ViewChild(Slides) slides: Slides;

	url: string;
	idModulo: string;
  nombreModulo: string;
  listaPreguntasBinarias: any;
  listaPreguntasMultiples: any;
  listaPreguntasRelacion: any;
	listaTips: any;
  preguntas: any = [];
  tips: any = [];
  modulo: any = [];
  respuestaPuntaje: any;
  idUsuario: any;
  idCurso: any;
  modulosUsuario: any;
  moduloSuperado: boolean = true;


  flashCardFlipped: boolean = false;

  habilitarCambioSlide: boolean;
  hasAnswered: boolean = true;
  msmCalificacion: string;


  habilitarContenidos: boolean = true;
  habilitarOpciones: boolean = true;
  habiltarOpcion: boolean = true;

  asignacionesContenidos: any = [];
  asignacionesOpciones: any = [];

  puntaje: number = 0;
  avance: number = 0;
  avanceAux : number = 0;
  puntajeAux: number = 0;

  //variable para ocultar el boton salir al finalizar el modulo
  terminado: boolean = false;

  numModuloActualExamen: number = 1;
  numModulosTotalExamen: number = 0;



  loading = this.loadingCtrl.create({
    content: 'Cargando módulo...'
  });

  constructor(platform: Platform, public navCtrl: NavController, public navParams: NavParams,  private servicio: Data, private alertCtrl: AlertController, public loadingCtrl: LoadingController, private nativeAudio: NativeAudio, public events: Events) {
        this.idModulo =  this.navParams.get('idModulo');
  	  	this.nombreModulo =  this.navParams.get('nombreModulo');

        this.idUsuario = JSON.parse(localStorage.getItem('usuarioDocumento'));
        this.idCurso = this.navParams.get('idCurso');

        this.nativeAudio.preloadSimple('respuestaCorrecta', 'assets/sounds/respuesta_correcta.mp3');
        this.nativeAudio.preloadSimple('respuestaIncorrecta', 'assets/sounds/respuesta_incorrecta.mp3');

        if (this.nombreModulo == "Examen") { 
          this.hasAnswered = false;
          this.obtenerPreguntasExamen();
        } else {
          this.obtenerListaPreguntasBinarias(this.idModulo);
        }

        this.obtenerModulosUsuario();
  }

	obtenerListaPreguntasBinarias(modulo){
    this.loading.present();
    let tipoPregunta: string;
		this.url = "https://cemexapp-168019.appspot.com/api/ili/preguntasbinarias/" + modulo;

		this.servicio.obtenerPreguntasBinarias(this.url)
			.subscribe(
				(data) => {
					this.listaPreguntasBinarias = data.preguntas;
          tipoPregunta = data.Tipo;
				},
				er => console.log(er),
				() => {
          for (var i = this.listaPreguntasBinarias.length - 1; i >= 0; i--) {
            this.preguntas.push(
                                { 
                                  id_pregunta: this.listaPreguntasBinarias[i].PREGUNTA_id, 
                                  atributos_pregunta : this.listaPreguntasBinarias[i],
                                  tipo_pregunta : tipoPregunta,
                                  peso: 1
                                }
                              );
          }
          this.obtenerListaPreguntasMultiples(modulo);
				}
			)	
	}

  obtenerListaPreguntasMultiples(modulo) {
    let tipoPregunta: string;
    this.url = "https://cemexapp-168019.appspot.com/api/ili/preguntasmultiples/" + modulo;

    this.servicio.obtenerPreguntasMultiples(this.url)
      .subscribe(
        (data) => {
          this.listaPreguntasMultiples = data.preguntas;
          tipoPregunta = data.Tipo;
        },
        er => console.log(er),
        () => {
          let opciones_respuesta : any = [];
          for (var i = this.listaPreguntasMultiples.length - 1; i >= 0; i--) {
            opciones_respuesta = [this.listaPreguntasMultiples[i].opcion_a,
                                  this.listaPreguntasMultiples[i].opcion_b,
                                  this.listaPreguntasMultiples[i].opcion_c,
                                  this.listaPreguntasMultiples[i].opcion_d];
            this.shuffle(opciones_respuesta);
            this.preguntas.push({ 
                                  id_pregunta: this.listaPreguntasMultiples[i].PREGUNTA_id, 
                                  atributos_pregunta : {  
                                        contenido : this.listaPreguntasMultiples[i].contenido,
                                        opciones : opciones_respuesta,
                                        opcion_correcta : this.listaPreguntasMultiples[i].opcion_a,
                                        imagen: this.listaPreguntasMultiples[i].imagen
                                      },
                                  tipo_pregunta : tipoPregunta,
                                  peso: 1
                                });
            opciones_respuesta = [];
          }
          this.preguntas.sort(function(a, b) {
            return parseInt(a.id_pregunta)-parseInt(b.id_pregunta);
          });
          this.obtenerListaPreguntasRelacion(modulo);
        }
      )  
  }

  obtenerListaPreguntasRelacion(modulo) {
    let tipoPregunta: string;
    this.url = "https://cemexapp-168019.appspot.com/api/ili/preguntasrelacion/" + modulo;

    this.servicio.obtenerPreguntasRelacion(this.url)
      .subscribe(
        (data) => {
          this.listaPreguntasRelacion = data.preguntas;
          tipoPregunta = data.Tipo;
        },
        er => console.log(er),
        () => {
          let contenidos_respuesta : any = [];
          let opciones_respuesta : any = [];

          for (var i = this.listaPreguntasRelacion.length - 1; i >= 0; i--) {

            contenidos_respuesta = [];
            opciones_respuesta = [];

            if(this.listaPreguntasRelacion[i].contenido_a) contenidos_respuesta.push({id : 'a', texto: this.listaPreguntasRelacion[i].contenido_a, seleccionado:false})
            if(this.listaPreguntasRelacion[i].contenido_b) contenidos_respuesta.push({id : 'b', texto: this.listaPreguntasRelacion[i].contenido_b, seleccionado:false})
            if(this.listaPreguntasRelacion[i].contenido_c) contenidos_respuesta.push({id : 'c', texto: this.listaPreguntasRelacion[i].contenido_c, seleccionado:false})
            if(this.listaPreguntasRelacion[i].contenido_d) contenidos_respuesta.push({id : 'd', texto: this.listaPreguntasRelacion[i].contenido_d, seleccionado:false})

            if(this.listaPreguntasRelacion[i].opcion_a) opciones_respuesta.push({id : 'a', texto: this.listaPreguntasRelacion[i].opcion_a, seleccionado:false})
            if(this.listaPreguntasRelacion[i].opcion_b) opciones_respuesta.push({id : 'b', texto: this.listaPreguntasRelacion[i].opcion_b, seleccionado:false})
            if(this.listaPreguntasRelacion[i].opcion_c) opciones_respuesta.push({id : 'c', texto: this.listaPreguntasRelacion[i].opcion_c, seleccionado:false})
            if(this.listaPreguntasRelacion[i].opcion_d) opciones_respuesta.push({id : 'd', texto: this.listaPreguntasRelacion[i].opcion_d, seleccionado:false})

            this.shuffle(contenidos_respuesta);
            this.shuffle(opciones_respuesta);
             
            this.preguntas.push(
                                { 
                                  id_pregunta: this.listaPreguntasRelacion[i].PREGUNTA_id, 
                                  atributos_pregunta : {
                                      contenido : this.listaPreguntasRelacion[i].contenido,
                                      contenidos : contenidos_respuesta,
                                      opciones : opciones_respuesta,
                                      imagen : this.listaPreguntasRelacion.imagen
                                  },
                                  tipo_pregunta : tipoPregunta,
                                  peso: 1
                                }
                              );
          }

          if (this.nombreModulo != "Examen") { 
            this.obtenerListaTips();
          }
          else {
            this.numModuloActualExamen++;


            if (this.numModulosTotalExamen == this.numModuloActualExamen) {
              this.modulo = this.preguntas;
              this.shuffle(this.modulo);
              this.modulo = this.modulo.slice(0, Math.ceil(this.modulo.length * 0.3));
              this.preguntas = this.preguntas.slice(0, Math.ceil(this.preguntas.length * 0.3));
              this.modulo.push({
                    id: 0,
                    id_pregunta: 0,
                    contenido : "Módulo Terminado",
                    imagen: null,
                    tipo_pregunta: "final"
                  });

            this.loading.dismiss();
            }
          }
        }
      )
  }

  obtenerListaTips() {
    this.url = "https://cemexapp-168019.appspot.com/api/ili/tips/" + this.idModulo;

    this.servicio.obtenerTips(this.url)
      .subscribe(
        (data) => {
          this.listaTips = data.tips;
        },
        er => console.log(er),
        () => {
          for (var i = this.listaTips.length - 1; i >= 0; i--) {
            this.tips.push(
                                { 
                                  id : this.listaTips[i].id,
                                  id_pregunta : this.listaTips[i].PREGUNTA_id, 
                                  contenido : this.listaTips[i].contenido,
                                  imagen : this.listaTips[i].imagen,
                                  tipo_pregunta : "Tip"
                                }
                              );
          }
          this.modulo = this.preguntas.concat(this.tips);

          this.modulo.sort(function(a, b) {

              if(a.id_pregunta === b.id_pregunta)
              {
                  var x = a.tipo_pregunta.toLowerCase(), y = b.tipo_pregunta.toLowerCase();

                  if(a.tipo_pregunta === "Tip" &&  b.tipo_pregunta === "Tip" ){

                    return a.id - b.id;
                  }
                  
                  return x > y ? -1 : x < y ? 1 : 0;
              }
              return a.id_pregunta - b.id_pregunta;
          });

          // Coloco elemento al final para poder ver un resumen de la partida y tambien para poder encolar todas la preguntas
          this.modulo.push({
                              id: 0,
                              id_pregunta: 0,
                              contenido : "Módulo Terminado",
                              imagen: null,
                              tipo_pregunta: "final"
                            });

          if (this.modulo[0].tipo_pregunta == "Tip" && this.modulo[0].imagen && this.modulo[0].imagen.includes("V") ){
            this.deshabilitarNext();
          } else if (this.modulo[0].tipo_pregunta == "Tip") { 
            this.slides.lockSwipeToNext(false);
          } else {
            this.slides.lockSwipeToNext(true);
          }
          this.loading.dismiss();

          // console.log(this.modulo);
        }
      )
  }

  responderBinaria(pregunta, respuesta) {
    this.hasAnswered = true;
    this.goToTop();

    if(pregunta.atributos_pregunta.respuesta == respuesta){
      this.nativeAudio.play('respuestaCorrecta');
      this.msmCalificacion = "Correcto"
      this.puntaje += (100/this.preguntas.length) * pregunta.peso;
      this.avance += 10/ this.preguntas.length;

    } else{
      this.nativeAudio.play('respuestaIncorrecta');
      this.msmCalificacion = "Incorrecto"
      this.encolarPregunta(pregunta);
      if (this.nombreModulo == "Examen") this.avance += 10/this.preguntas.length;

    }
    
    this.puntajeAux = Math.trunc(this.puntaje)
    this.avanceAux = Math.trunc(this.avance)
    this.slides.lockSwipeToNext(false);
    this.flashCardFlipped = true;
  }

  responderMultiple(pregunta, seleccion, respuesta) {
    this.hasAnswered = true;
    this.goToTop();

    if (seleccion == respuesta) { 
      this.nativeAudio.play('respuestaCorrecta');
      this.msmCalificacion = "Correcto";
      this.puntaje += (100/this.preguntas.length) * pregunta.peso;
      this.avance += 10/ this.preguntas.length;

    } else {
      this.nativeAudio.play('respuestaIncorrecta');
      this.msmCalificacion = "Incorrecto"
      this.encolarPregunta(pregunta);
      if (this.nombreModulo == "Examen") this.avance += 10/ this.preguntas.length;
    }

    this.puntajeAux = Math.trunc(this.puntaje)
    this.avanceAux = Math.trunc(this.avance)
    this.slides.lockSwipeToNext(false);
    this.flashCardFlipped = true;
  }

  responderRelacion(pregunta){
     
    this.hasAnswered = true;
    this.goToTop();
    var incorrecto = false;

    for( var i = 0; i < this.asignacionesContenidos.length; i++ ){
      if( this.asignacionesContenidos[i] !== this.asignacionesOpciones[i] ) {
        incorrecto = true;
        break;
      }
    }

    if(!incorrecto){ 
      this.nativeAudio.play('respuestaCorrecta');
      this.msmCalificacion = "Correcto"
      this.puntaje += (100/this.preguntas.length) * pregunta.peso;
      this.avance += 10/ this.preguntas.length;

    } 
    else {
      this.nativeAudio.play('respuestaIncorrecta');
      this.msmCalificacion = "Incorrecto"
      this.encolarPregunta(pregunta);
      if (this.nombreModulo == "Examen") this.avance += 10/ this.preguntas.length;
    }
    
    this.puntajeAux = Math.trunc(this.puntaje)
    this.avanceAux = Math.trunc(this.avance)
    this.slides.lockSwipeToNext(false);
    this.flashCardFlipped = true;

    this.reiniciarRelacion(pregunta);
    this.habilitarContenidos = false;
    this.habilitarOpciones = false;

  }

  siguienteSlide() {
    this.slides.slideNext();
    this.slides.lockSwipeToNext(false);
  }

  cambioSlide() {
    let existeModuloUsuario: boolean = false;

    this.flashCardFlipped = false;
    if (this.modulo[this.slides.getActiveIndex()].tipo_pregunta == "Tip") {

      if ( this.modulo[this.slides.getActiveIndex()].imagen && this.modulo[this.slides.getActiveIndex()].imagen.includes("V") ){
        this.deshabilitarNext();
      } else {

      this.habilitarCambioSlide = false;
      this.slides.lockSwipeToNext(this.habilitarCambioSlide);
      this.hasAnswered = true;

      }

    } 
    else if(this.modulo[this.slides.getActiveIndex()].tipo_pregunta == "final"){
      if (this.puntaje < 80) {
        this.moduloSuperado = false;
      }

      if (this.slides.isEnd() ) {
        for (var i = 0; i < this.modulosUsuario.length; ++i) {
          if (this.modulosUsuario[i].MODULO_id == this.idModulo) {
            existeModuloUsuario = true;
            if (this.modulosUsuario[i].valor < this.puntaje) {
              this.puntajeAux = Math.ceil(this.puntajeAux)
              this.actualizarPuntaje(this.puntajeAux , this.idModulo, this.idUsuario);
              break;
            }
          }
        }
        if (existeModuloUsuario == false) {
          this.puntajeAux = Math.ceil(this.puntajeAux)
          this.actualizarPuntaje(this.puntajeAux, this.idModulo, this.idUsuario);
        }
      }
      this.terminado = true;
      //console.log(this.puntaje);
    }

    else {
      this.flashCardFlipped = false;
      this.habilitarCambioSlide = true;
      this.slides.lockSwipeToNext(this.habilitarCambioSlide);
      this.hasAnswered = false;
    }

    this.habilitarContenidos = true;
    this.habilitarOpciones = true;

    this.goToTop();

  }

  goToTop(){
    this.content.scrollTo(0,0,1);
  }

  /**
  * Shuffles array in place. ES6 version
  */
  shuffle(a) {

      for (let i = a.length; i; i--) {
          let j = Math.floor(Math.random() * i);
          [a[i - 1], a[j]] = [a[j], a[i - 1]];
      }
  }

  seleccionRelacion(pregunta, item, tipo, arreglo){

    if(tipo == "contenidos" && this.habilitarOpciones == true){
      this.habilitarContenidos = false;
      this.asignacionesContenidos.push(item.id);

    } else if(tipo == "contenidos" && this.habilitarOpciones == false){
      this.habilitarOpciones = true;
      this.asignacionesContenidos.push(item.id);

    } else if(tipo == "opciones" && this.habilitarContenidos == true) {
      this.habilitarOpciones = false;
      this.asignacionesOpciones.push(item.id);

    } else if(tipo == "opciones" && this.habilitarContenidos == false) {
      this.habilitarContenidos = true;
      this.asignacionesOpciones.push(item.id);
    }

    item.seleccionado = true;

    if (this.asignacionesContenidos.length == arreglo.length && this.asignacionesOpciones.length == arreglo.length){
      this.responderRelacion(pregunta)
    }

  }

  reiniciarRelacion(item){

    for(var i = 0; i < item.atributos_pregunta.contenidos.length; i++){
       item.atributos_pregunta.contenidos[i].seleccionado = false;
    }

    for(var i = 0; i < item.atributos_pregunta.opciones.length; i++){
       item.atributos_pregunta.opciones [i].seleccionado = false;
    }

    this.asignacionesContenidos = [];
    this.asignacionesOpciones = [];
    this.habilitarContenidos = true;
    this.habilitarOpciones = true;
  }

  encolarPregunta(pregunta){
    if (this.nombreModulo != "Examen") {
      let aux = pregunta;
      aux.peso -= pregunta.peso * 0.7; 

      this.modulo.splice(this.modulo.length - 1, 0 , aux)
    }
  }

  salirModulo(){
    this.alertSalirModulo();
  }

  alertSalirModulo(){
    let alert = this.alertCtrl.create({
      title: 'Salir',
      message: "Cancelará el módulo actual y deberá volverlo a iniciar.",
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            //this.navCtrl.setRoot(TabsPage);
            this.events.publish('reloadModules', this.idCurso);
            this.navCtrl.pop()
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
        }
      ]
    });
    alert.present();
  }

  finalizarModulo(){
    //this.navCtrl.setRoot(TabsPage);
    this.events.publish('reloadModules', this.idCurso);
    this.navCtrl.pop()
  }

  actualizarPuntaje(valor, MODULO_id, USUARIO_id) {
    ///console.log(valor);
    //console.log(MODULO_id);
    //console.log(USUARIO_id);
    this.url = "https://cemexapp-168019.appspot.com/api/ili/puntaje";
    this.servicio.actualizarPuntaje(this.url, valor, MODULO_id, USUARIO_id)
      .subscribe(
        (data) => {
          this.respuestaPuntaje = data;
        },
        er => console.log(er),
        () => {
          console.log(this.respuestaPuntaje);
        }
      )
  }

  deshabilitarNext() {
      this.hasAnswered = false;
      this.slides.lockSwipeToNext(true); 
  }

  videoEnded() {
    this.hasAnswered = true;
    this.slides.lockSwipeToNext(false);
  }
    
  obtenerModulosUsuario() {
    this.url = "https://cemexapp-168019.appspot.com/api/ili/modulosCursoConPuntaje/" + this.idUsuario + "/" + this.idCurso;

    this.servicio.obtenerModulos(this.url)
      .subscribe(
        (data) => {
          this.modulosUsuario = data.Modulos;
        },
        er => console.log(er),
        () => {
        }
      )
  }

  obtenerPreguntasExamen() {
    let urlModulos;
    let modulos;

    urlModulos = "https://cemexapp-168019.appspot.com/api/ili/modulos/" + this.idCurso;

    this.servicio.obtenerModulos(urlModulos)
      .subscribe(
        (data) => {
          modulos = data.Modulos;
        },
        er => console.log(er),
        () => {
          this.numModulosTotalExamen = modulos.length;

          for (var i = 0; i < modulos.length-1; ++i) {
            this.obtenerListaPreguntasBinarias(modulos[i].id);
          }
        }
      )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartidaPage');
    this.slides.lockSwipeToPrev(true);
  }

}

