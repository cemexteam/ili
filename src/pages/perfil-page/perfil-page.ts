import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ActionSheetController, App } from 'ionic-angular';

import { Data } from '../../providers/data'

import { AsignacionCursosPage } from '../asignacion-cursos/asignacion-cursos';


/**
 * Generated class for the PerfilPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-perfil-page',
  templateUrl: 'perfil-page.html',
})
export class PerfilPage {

	usuario : any;
	nombre : string;
	apellido : string;
	sexo : any;
	tipoEmpleado : any;
	url: string;
	respuesta: any;


	constructor(public navCtrl: NavController, public navParams: NavParams,  private alertCtrl: AlertController, private servicio: Data, public actionSheetCtrl: ActionSheetController, private app: App) {

		this.usuario =  JSON.parse(localStorage.getItem('usuario'));

		this.nombre =  this.usuario.nombre;
		this.apellido =  this.usuario.apellido;

		if(this.usuario.tipo_empleado) this.tipoEmpleado = {display: this.displayTipoEmpleado(this.usuario.tipo_empleado), value:this.usuario.tipo_empleado};
		else{this.usuario.tipo_empleado = this.tipoEmpleado = {display: this.displayTipoEmpleado(localStorage.getItem('usuarioTipoEmpleado')), value:localStorage.getItem('usuarioTipoEmpleado')};}
		this.sexo = this.usuario.sexo;

	}

	displayTipoEmpleado(param){

		console.log(param)

		switch (param) {
			case "comercial_tecnica":
				return "Comercial y Técnica"
			
			case "operacion":
				return "Operación"

			case "agente_servicio":
				return "Agente de Servicio"
		}
	}

	actualizarTipoEmpleado(cemexId, tipoEmpleado) {
	this.url = "https://cemexapp-168019.appspot.com/api/ili/usuario/tipo_empleado";
	console.log(cemexId);
	this.servicio.actualizarTipoEmpleado(this.url, cemexId, tipoEmpleado)
		.subscribe(
			(data) => {
				this.respuesta = data;
			},
			er => console.log(er),
			() => {
				console.log(this.respuesta);
			}
		)

	}  

  	alertTipoEmpleado() {
		let alert = this.alertCtrl.create();
		alert.setTitle('Tipo de Empleado');

		alert.addInput({
			type: 'radio',
			label: 'Comercial y Técnica',
			value: 'comercial_tecnica',
			checked: true
		});
		alert.addInput({
			type: 'radio',
			label: 'Operación',
			value: 'operacion',
			checked: false
		});
		alert.addInput({
			type: 'radio',
			label: 'Agente de Servicio',
			value: 'agente_servicio',
			checked: false
		});

		alert.addButton('Cancelar');
		alert.addButton({
			text: 'Ok',
			handler: data => {
				this.tipoEmpleado = data;
				this.actualizarTipoEmpleado(this.usuario.cemex_id, this.tipoEmpleado);
				this.tipoEmpleado = {display: this.displayTipoEmpleado(data), value: data};
			}
		});
		
		alert.present();
	}

	presentActionSheet() {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Opciones',
			buttons: [
				{
					text: 'Cambiar Tipo de Empleado',
					handler: () => {
						this.alertTipoEmpleado();
					}
				},{
					text: 'Asignar Cursos',
					handler: () => {
						this.app.getRootNav().push(AsignacionCursosPage);
						// this.navCtrl.setRoot(AsignacionCursosPage);
					}
				},{
					text: 'Cancelar',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		actionSheet.present();
	}
}
