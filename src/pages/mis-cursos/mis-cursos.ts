import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { Data } from '../../providers/data'
import { NativeAudio } from '@ionic-native/native-audio';
import { ModulosPage } from '../modulos-page/modulos-page';
/**
 * Generated class for the MisCursos page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-mis-cursos',
  templateUrl: 'mis-cursos.html',
})
export class MisCursos {

	idUsuario: any;
	cursosUsuario: any = [];
	grid : any;
	url: string;

	loading = this.loadingCtrl.create({
		content: 'Cargando...'
	});


	constructor(public navCtrl: NavController, public navParams: NavParams,  private servicio: Data, public loadingCtrl: LoadingController, private nativeAudio: NativeAudio) {

		this.idUsuario =  JSON.parse(localStorage.getItem('usuarioDocumento'));

	}

	ionViewDidEnter(){

		this.loading = this.loadingCtrl.create({
			content: 'Cargando...'
		});		


		this.verListaCursosUsuario();
		this.construirGrid();


	}

	incrementar10(){

/*		if(this.loadProgress < 100) this.loadProgress+=1;
		setTimeout(() => {this.incrementar10()}, 30);*/
	}	

	verListaCursosUsuario(){

		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/avanceCursosAsignados/" + this.idUsuario;
		this.servicio.obtenerCursosUsuario(this.url)
			.subscribe(
				(data) => {
					this.cursosUsuario = data.Avance;
				},
				er => console.log(er),
				() => {
					for (var i = 0; i < this.cursosUsuario.length; ++i) {

						if( this.cursosUsuario[i].progreso ) this.cursosUsuario[i].progreso = Math.ceil(this.cursosUsuario[i].progreso) ;

						else{ this.cursosUsuario[i].progreso = 0; }
				}
					this.loading.dismiss();
					this.construirGrid();
				}
			)	


	}

	construirGrid() {
	  	this.grid = Array(Math.ceil(this.cursosUsuario.length/2));

	  	let rowNum = 0;

	  	for(let i = 0; i < this.cursosUsuario.length; i+=2){

	  		this.grid[rowNum] =  Array(2);

	  		if(this.cursosUsuario[i]){
	  			this.grid[rowNum][0] = this.cursosUsuario[i];
	  		}

	  		if(this.cursosUsuario[i+1]){
	  			this.grid[rowNum][1] = this.cursosUsuario[i+1]
	  		}
	  		
	  		rowNum++;
	  	}
  	}


  	verModulos(curso) {
		this.nativeAudio.play('clickCurso');

		let data = {
			idCurso : curso
		};
		this.navCtrl.push(ModulosPage, data);
	}

}
