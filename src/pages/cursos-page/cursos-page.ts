import { Component } from '@angular/core';
import { Platform, NavController, NavParams, LoadingController } from 'ionic-angular';

import { ModulosPage } from '../modulos-page/modulos-page';
import { Data } from '../../providers/data';

import { NativeAudio } from '@ionic-native/native-audio';

/**
 * Generated class for the SeguridadPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-cursos-page',
  templateUrl: 'cursos-page.html',
})
export class CursosPage {

	url : string;
	idCategoria : string;
	listaCursos : any;
	grid : any;

	loading = this.loadingCtrl.create({
		content: 'Cargando cursos...'
	});

	constructor(platform: Platform, public navCtrl: NavController, public navParams: NavParams, private servicio: Data, private nativeAudio: NativeAudio, public loadingCtrl: LoadingController) {

		this.idCategoria = this.navParams.get('idCategoria');

	}


	ionViewDidEnter(){

		this.loading = this.loadingCtrl.create({
			content: 'Cargando...'
		});		

		this.verListaCursos();
		this.nativeAudio.preloadSimple('clickCurso', 'assets/sounds/clickCurso.mp3');


	}	

	verListaCursos(){
		//console.log(this.idCategoria);
		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/curso/" + this.idCategoria;

		this.servicio.obtenerCursos(this.url)
			.subscribe(
				(data) => {
					this.listaCursos = data.Cursos;
				},
				er => console.log(er),
				() => {
					//console.log(this.listaCursos);
					this.construirGrid();
					this.loading.dismiss();
				}
			)	
	}

	verModulos(curso) {
		this.nativeAudio.play('clickCurso');

		let data = {
			idCurso : curso
		};
		this.navCtrl.push(ModulosPage, data);
	}


	construirGrid() {
	  	this.grid = Array(Math.ceil(this.listaCursos.length/2));

	  	let rowNum = 0;

	  	for(let i = 0; i < this.listaCursos.length; i+=2){

	  		this.grid[rowNum] =  Array(2);

	  		if(this.listaCursos[i]){
	  			this.grid[rowNum][0] = this.listaCursos[i];
	  		}

	  		if(this.listaCursos[i+1]){
	  			this.grid[rowNum][1] = this.listaCursos[i+1]
	  		}
	  		
	  		rowNum++;
	  	}
  }

}














