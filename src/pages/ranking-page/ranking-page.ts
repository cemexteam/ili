import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { RankingListadoPage } from '../rankingListado-page/rankingListado-page';

import { Data } from '../../providers/data'

/**
 * Generated class for the RankingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-ranking-page',
  templateUrl: 'ranking-page.html',
})
export class RankingPage {

	cursosGeneral: any;
	cursosGeneralSearch: any;
	cursosUsuario: any;
	cursosUsuarioSearch: any;
	rankingTotal: any;
	rankingTotalSearch: any;
	idCurso: any;
	idUsuario: any;
	url: string;
	rank: string = "total";
	modulosUsuario: any = [];
	numModulos: number = 0;


	loading = this.loadingCtrl.create({
		content: 'Cargando...'
	});

	constructor(public navCtrl: NavController, public navParams: NavParams, private servicio: Data, public loadingCtrl: LoadingController) {
		this.idUsuario =  JSON.parse(localStorage.getItem('usuarioDocumento'));
	
	}

	ionViewDidEnter(){
		this.loading = this.loadingCtrl.create({
			content: 'Cargando...'
		});

		this.verListaCursosUsuario();
		this.numModulos = 0;
		this.modulosUsuario = [];
	}

	verListaCursosUsuario(){

		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/cursosUsuario/" + this.idUsuario;
		this.servicio.obtenerCursosUsuario(this.url)
			.subscribe(
				(data) => {
					this.cursosUsuario = data.Cursos;
				},
				er => console.log(er),
				() => {
					for (var i = 0; i < this.cursosUsuario.length; ++i) {
						this.cursosUsuario[i].aprobado = false;
					}
					if (this.cursosUsuario.length == 0) {
						this.verListaCursos();
					}
					for (var j = 0; j < this.cursosUsuario.length; ++j) {
						this.modulosCursoPuntaje(this.cursosUsuario[j].id);
					}
				}
			)	
	}

	verListaCursos(){
		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/cursos";
		this.servicio.obtenerCursos(this.url)
			.subscribe(
				(data) => {
					this.cursosGeneral = data.Cursos;
				},
				er => console.log(er),
				() => {
					this.verRankingTotal();
				}
			)	
	}

	verRankingTotal(){
		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/rankingTotal";
		this.servicio.obtenerRankingTotal(this.url)
			.subscribe(
				(data) => {
					this.rankingTotal = data.Ranking;
				},
				er => console.log(er),
				() => {
					for (var i = 0; i < this.rankingTotal.length; ++i) {
						this.rankingTotal[i].posicion = i + 1;
					}
					this.initializeItems();
					this.loading.dismiss();
				}
			)	
	}

	initializeItems() {
		this.cursosGeneralSearch = this.cursosGeneral;
		this.cursosUsuarioSearch = this.cursosUsuario;
		this.rankingTotalSearch = this.rankingTotal;
	}

	getItems(ev: any, rank) {
		this.initializeItems();

		let val = ev.target.value;

		if (rank == "total") {
			if (val && val.trim() != '') {
				this.rankingTotalSearch = this.rankingTotalSearch.filter((item) => {
					return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
				})
			}
		}
		if (rank == "general") {
			if (val && val.trim() != '') {
				this.cursosGeneralSearch = this.cursosGeneralSearch.filter((item) => {
					return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
				})
			}
		}
		if (rank == "misCursos") {
			if (val && val.trim() != '') {
				this.cursosUsuarioSearch = this.cursosUsuarioSearch.filter((item) => {
					return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
				})
			}
		}
	}

	modulosCursoPuntaje(idCurso) {
		let modulosCursoUsuario;

		this.url = "https://cemexapp-168019.appspot.com/api/ili/modulosCursoConPuntaje/" + this.idUsuario + "/" + idCurso;

		this.servicio.obtenerModulos(this.url)
			.subscribe(
				(data) => {
					modulosCursoUsuario = data.Modulos;
				},
				er => console.log(er),
				() => {
					this.numModulos++;
					this.modulosUsuario = this.modulosUsuario.concat(modulosCursoUsuario);
					if (this.cursosUsuario.length == this.numModulos) {
						console.log(this.modulosUsuario);
						this.verificacionExamenes();
					}
				}
			)
	}


	verificacionExamenes() {

		let cursosAprobados = [];

		for (var i = 0; i < this.modulosUsuario.length; ++i) {
			
			if(this.modulosUsuario[i].nombre.includes("Examen") && this.modulosUsuario[i].valor >= 80){
            
       			cursosAprobados.push(this.modulosUsuario[i].CURSO_id);
			}
		}

		for (var i = 0; i < cursosAprobados.length; ++i) {

			for(var j = 0 ; j < this.cursosUsuario.length; j++){

				if( cursosAprobados[i] == this.cursosUsuario[j].id ) {
					this.cursosUsuario[j].aprobado = true;
					break;
				}
			}

		}
		this.verListaCursos();
		console.log(this.cursosUsuario)
	}

	verPaginaListadoRanking(curso, nombre){
		let data = {
	  		idCurso : curso,
	  		nombreCurso : nombre
	  	};


		this.navCtrl.push(RankingListadoPage, data);
	}
}
