import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { MasparatiPage } from '../masparati/masparati';
import { TabsPage } from '../tabs-page/tabs-page';

@Component({
  selector: 'page-seleccion',
  templateUrl: 'seleccion.html'
})

export class SeleccionPage {
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  getTabsPage() {
    this.navCtrl.push(TabsPage);
  }

  // itemTapped(event, item) {
  //   this.navCtrl.push(ItemDetailsPage, {
  //     item: item
  //   });
  // }
}
