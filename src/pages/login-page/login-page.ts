import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { TabsPage } from '../tabs-page/tabs-page';
import { Data } from '../../providers/data'

import 'rxjs/add/operator/map';

import { ConnectivityServiceProvider } from '../../providers/connectivity-service/connectivity-service';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-page',
  templateUrl: 'login-page.html',
})
export class LoginPage {

  	cemexId: string;
	n_empleado: string;
	respuesta: any;
	url: string;
	tipoEmpleado: string;
	message: string;

	loading = this.loadingCtrl.create({
		content: 'Autenticando...'
	});

	private loginForm : FormGroup;

	constructor(public navCtrl: NavController, public navParams: NavParams, private servicio: Data, private alertCtrl: AlertController, public formBuilder: FormBuilder, public connectivityService: ConnectivityServiceProvider, public loadingCtrl: LoadingController) {

		this.loginForm = this.formBuilder.group({
			cemexId:['', Validators.required],
			n_empleado: ['', Validators.required],
		});

		if( localStorage.getItem('usuarioCemexId') && localStorage.getItem('usuarioNEmpleado') ){
			this.cemexId = localStorage.getItem('usuarioCemexId').replace(/"/g,'');
			this.n_empleado = localStorage.getItem('usuarioNEmpleado').replace(/"/g,'');
		}
	}

	verHomePage() {

	    this.url = "https://cemexapp-168019.appspot.com/api/ili/usuario/" + this.cemexId + "/" + this.n_empleado;
		if(this.connectivityService.isOnline()){
			this.loading.present();
			this.url = "https://cemexapp-168019.appspot.com/api/ili/usuario/" + this.cemexId + "/" + this.n_empleado;
			this.servicio.getUsuario(this.url)
				.subscribe(
					(data) => {
						this.respuesta = data;
						// Si el cemexId y el No. de empleado en el login no corresponden a un usuario en la BD
						if (this.respuesta.UsersNumber == 0) {
							this.alertLoginInvalido();
						}

						// Si el cemexId y el No. de empleado en el login corresponden a un usuario en la BD
						if (this.respuesta.UsersNumber == 1) {
							//se guarda el cemexId para conservar al sesion.
							localStorage.setItem('usuarioCemexId', JSON.stringify(this.respuesta.Users[0].cemex_id) )
							localStorage.setItem('usuarioNEmpleado', JSON.stringify(this.respuesta.Users[0].n_empleado) )
							localStorage.setItem('usuarioId', JSON.stringify(this.respuesta.Users[0].id) )
							localStorage.setItem('usuario', JSON.stringify(this.respuesta.Users[0]) )
							localStorage.setItem('usuarioDocumento', JSON.stringify(this.respuesta.Users[0].n_documento) )
							//console.log(localStorage.getItem('usuarioCemexId'))

							// Se verifica tipo de empleado del usuario para asignarlo en caso que sea nulo
							if (this.respuesta.Users[0].tipo_empleado == null || this.respuesta.Users[0].tipo_empleado == "") {
								this.alertTipoEmpleado();	
						
							}
							else {
								this.navCtrl.setRoot(TabsPage);
							}
						}
					},
					er => console.log(er),
					() => {
						//console.log(this.respuesta);
						this.loading.dismiss();
						this.loading = this.loadingCtrl.create({
							content: 'Autenticando...'
						});
					}
				)
		    }
		    else {
		      this.alertSinConexion();
		    }
		}

	actualizarTipoEmpleado(cemexId, tipoEmpleado) {
		this.url = "https://cemexapp-168019.appspot.com/api/ili/usuario/tipo_empleado";
		console.log(cemexId);
		this.servicio.actualizarTipoEmpleado(this.url, cemexId, tipoEmpleado)
			.subscribe(
				(data) => {
					this.respuesta = data;
				},
				er => console.log(er),
				() => {
					localStorage.setItem('usuarioTipoEmpleado', tipoEmpleado);
					
					console.log(this.respuesta);
				}
			)

	}

	alertSinConexion(){

		let alert = this.alertCtrl.create({
			title:"Sin Conexión",
			message:"No hay conexión",
			buttons: ['Ok']

		});

		alert.present();
	}	

	alertLoginInvalido(){

		let alert = this.alertCtrl.create({
			title:"Acceso Denegado",
			message:"Credenciales inválidas",
			buttons: ['Ok']

		});

		alert.present();
	}

	alertTipoEmpleado() {
		let alert = this.alertCtrl.create();
		alert.setTitle('Tipo de Empleado');

		alert.addInput({
			type: 'radio',
			label: 'Comercial y Técnica',
			value: 'comercial_tecnica',
			checked: true
		});
		alert.addInput({
			type: 'radio',
			label: 'Operación',
			value: 'operacion',
			checked: false
		});
		alert.addInput({
			type: 'radio',
			label: 'Agente de Servicio',
			value: 'agente_servicio',
			checked: false
		});

		alert.addButton('Cancelar');
		alert.addButton({
			text: 'Ok',
			handler: data => {
				this.tipoEmpleado = data;
				this.actualizarTipoEmpleado(this.cemexId, this.tipoEmpleado);
				this.navCtrl.setRoot(TabsPage);
			}
		});
		
		alert.present();
	}

}




















