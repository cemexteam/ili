import { Component } from '@angular/core';

import { JuegoPage } from '../juego-page/juego-page';
import { RankingPage } from '../ranking-page/ranking-page';
import { PerfilPage } from '../perfil-page/perfil-page';
import { MisCursos } from '../mis-cursos/mis-cursos';

/**
 * Generated class for the TabsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage {

	tab1Root = JuegoPage;
	tab2Root = RankingPage;
	tab3Root = PerfilPage;
	tab4Root = MisCursos;

	constructor() {
	}
}
