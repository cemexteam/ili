import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { Data } from '../../providers/data'

/**
 * Generated class for the RankingListadoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-rankingListado-page',
  templateUrl: 'rankingListado-page.html',
})
export class RankingListadoPage {

	ranking: any;
	idCurso: any;
	nombreCurso: any;
	url: string;

	loading = this.loadingCtrl.create({
		content: 'Cargando...'
	});

	constructor(public navCtrl: NavController, public navParams: NavParams, private servicio: Data, public loadingCtrl: LoadingController) {
		this.idCurso = this.navParams.get('idCurso');
		this.nombreCurso = this.navParams.get('nombreCurso');
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad RankingListadoPage');
	}

	ionViewDidEnter(){
		this.loading = this.loadingCtrl.create({
			content: 'Cargando...'
		});

		this.verRanking();
	}

	verRanking(){
		this.loading.present();
		this.url = "https://cemexapp-168019.appspot.com/api/ili/ranking/" + this.idCurso;
		this.servicio.obtenerRanking(this.url)
			.subscribe(
				(data) => {
					this.ranking = data.Ranking;
				},
				er => console.log(er),
				() => {
					this.loading.dismiss();
				}
			)	
	}

}
