import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';

/*
  Generated class for the Data provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Data {

	private options;

	constructor(public http: Http) {
	}

	getUsuario(url){
		return this.http.get(url, this.options)
					.map(r => r.json())
					.catch(this.handleError);
	} 

	obtenerCursos(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	} 

	obtenerModulos(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	} 	

	obtenerPreguntasBinarias(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	} 	

	obtenerPreguntasMultiples(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}

	obtenerPreguntasRelacion(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}

	obtenerTips(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}

	actualizarTipoEmpleado(url, cemexId, tipoEmpleado){
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
		headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

		let body = {
			cemex_id: cemexId,
			tipo_empleado: tipoEmpleado,
		}

		return this.http.put(url, JSON.stringify(body), {headers: headers})
			.map(r => r.json())
			.catch(this.handleError);
	}

	actualizarPuntaje(url, valor, modulo_id, usuario_id){
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
		headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

		let body = {
			valor: valor,
			MODULO_id: modulo_id,
			USUARIO_id: usuario_id
		}

		return this.http.put(url, JSON.stringify(body), {headers: headers})
			.map(r => r.json())
			.catch(this.handleError);
	}

	obtenerRanking(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}

	obtenerCursosUsuario(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}

	verificarModulosHabilitados(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}
	
	obtenerRankingTotal(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}

	obtenerUsuarios(url){
	return this.http.get(url, this.options)
				.map(r => r.json())
				.catch(this.handleError);
	}

	asignarCursos(url, resultado){
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Access-Control-Allow-Origin', '*');
		headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

		let body = {
			ASIGNACIONES_CURSOS: resultado
		}

		return this.http.put(url, JSON.stringify(body), {headers: headers})
			.map(r => r.json())
			.catch(this.handleError);
	}

	private handleError ( error: Response | any) {
		let errMsg : string;
		if(error instanceof Response) { 
			const body = error.json() || '';
			const err = body.error || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		console.error(errMsg);
		return Observable.throw(errMsg);
	}

}
